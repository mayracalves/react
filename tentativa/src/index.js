import React from 'react';//necesario para todas as paginas que forem utilizar jsx, desse jeito por exemplo <App />
import ReactDOM from 'react-dom';//eh para fazer o .render funcionar
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

serviceWorker.unregister();
